---
title : "Fictieve Belastingsamenwerking - Mijn Omgeving - backend"
description: "Backend for the Fictieve Belastingsamenwerking - Mijn Omgeving Flutter app. Shows messages for scoped to the WOZ."
date: 2023-10-27T09:14:40+02:00
draft: false
toc: true
---

## Running locally

Clone this repo.

Run:

```sh
go run main.go
```

This starts a web server locally on the address specified in the config.
