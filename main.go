package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-omgeving-backend/backend"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-omgeving-backend/config"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-omgeving-backend/model"
)

type personWithMunicipality struct {
	model.Person
	MunicipalityID   string `json:"municipalityID"` // IMPROVE: using a struct, or pass as extra data. Done like this for simplicity
	MunicipalityName string `json:"municipalityName"`
}

type message struct {
	ID          uuid.UUID    `json:"id"`
	Title       string       `json:"title"`
	Content     string       `json:"content,omitempty"`
	CreatedAt   time.Time    `json:"createdAt"`
	Attachments []attachment `json:"attachments,omitempty"`
}

type attachment struct {
	Label string `json:"label"`
	URL   string `json:"url"`
}

func main() {
	// Fiber instance
	app := fiber.New()

	// Middleware
	app.Use(logger.New(logger.Config{
		Format: "${time} | ${status} | ${latency} | ${method} | ${path}   ${error}\n", // Do not log IP addresses. Note: see https://docs.gofiber.io/api/middleware/logger#constants for more logger variables
	}))

	// Allow requests from all origins
	app.Use(cors.New(cors.Config{
		// AllowOrigins: "*", // Default value
		AllowMethods: "GET, POST, HEAD, PUT, DELETE, PATCH, OPTIONS", // Note: OPTIONS is not included by default
	}))

	// security.txt redirect
	app.Get("/.well-known/security.txt", func(c *fiber.Ctx) error {
		return c.Redirect("https://www.ncsc.nl/.well-known/security.txt", fiber.StatusFound) // StatusFound is HTTP code 302
	})

	app.Get("/", func(c *fiber.Ctx) error {
		return nil
	})

	// People
	people := app.Group("/people")
	people.Get("/", func(c *fiber.Ctx) error {
		// Fetch the people from the Fictief Register Personen backends
		query := url.Values{}
		// query.Set("perPage", c.Query("perPage", "10"))
		// query.Set("lastId", c.Query("lastId"))
		// query.Set("firstId", c.Query("firstId"))

		var people []personWithMunicipality

		for _, municipality := range config.Config.Municipalities {
			response := new(model.Response[model.Person])
			if err := backend.Request(http.MethodGet, municipality.FRP.Domain, fmt.Sprintf("/people?%s", query.Encode()), nil, map[string]string{"Fsc-Grant-Hash": municipality.FRP.FSCGrantHash}, response); err != nil {
				return fmt.Errorf("error fetching people : %w", err)
			}

			for _, resp := range response.Data {
				people = append(people, personWithMunicipality{
					resp,
					municipality.ID,
					municipality.Name,
				})
			}
		}

		return c.JSON(people)
	})

	// Messages
	messages := people.Group("/:municipalityID/:personID/messages")
	messages.Get("/", func(c *fiber.Ctx) error {
		// // Get the specified municipality
		// municipality, err := config.GetMunicipalityByID(c.Params("municipalityID"))
		// if err != nil {
		// 	return err
		// }

		// Fetch all tax bills for this person. IMPROVE: support pagination
		query := url.Values{}
		// query.Set("perPage", c.Query("perPage", "10"))
		// query.Set("lastId", c.Query("lastId"))
		// query.Set("firstId", c.Query("firstId"))

		// Note: we assume the personID is unique across all municipalities. IMPROVE: take the municipality into account?

		response := new(model.Response[model.TaxBill])
		if err := backend.Request(http.MethodGet, config.Config.Belastingsamenwerking.Domain, fmt.Sprintf("/owners/%s/tax-bills?%s", c.Params("personID"), query.Encode()), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Belastingsamenwerking.FSCGrantHash}, response); err != nil {
			return fmt.Errorf("error fetching tax bills: %w", err)
		}

		// For each tax bill for this person, generate a message on the fly, since we do not store messages in this PoC
		messages := make([]message, len(response.Data))
		for i, bill := range response.Data {
			messages[i] = message{
				ID: bill.ID, // Note: use the tax bill ID as message ID for now
				Title: fmt.Sprintf(
					"WOZ-aanslag %d",
					bill.CreatedAt.Year(), // TODO: use EffectiveAt once fixed in the backend?
				),
				CreatedAt: bill.CreatedAt,
			}
		}

		return c.JSON(messages)
	})

	messages.Get("/:messageID", func(c *fiber.Ctx) error {
		// Parse the message ID
		messageID, err := uuid.Parse(c.Params("messageID"))
		if err != nil {
			return fmt.Errorf("unrecognized message ID: %w", err)
		}

		// Get the assement from the backend. Note: in this PoC, the message ID equals the tax bill ID
		bill := new(model.TaxBill)
		if err := backend.Request(http.MethodGet, config.Config.Belastingsamenwerking.Domain, fmt.Sprintf("/tax-bills/%s", messageID), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Belastingsamenwerking.FSCGrantHash}, bill); err != nil {
			return fmt.Errorf("tax bill request failed: %w", err)
		}

		// Generate a message on the fly since we do not store messages in this PoC
		return c.JSON(message{
			ID: messageID,
			Title: fmt.Sprintf(
				"WOZ-aanslag %d",
				bill.CreatedAt.Year(), // TODO: use EffectiveAt once fixed in the backend?
			),
			CreatedAt: bill.CreatedAt,
			Content: fmt.Sprintf(`U ontvangt van %s de belastingaanslag gemeente-/waterschapsbelastingen/WOZ. Deze belastingaanslag vindt u in de bijlage bij dit bericht.

Wat is %s?
%s heft en int belastingen voor gemeenten en waterschappen. Voor gemeenten stelt %s ook de WOZ-waardes vast van alle woningen en bedrijven. %s voert deze taken uit voor:
- Gemeente Woudendijk
- Gemeente Zonnendaal

Heeft u vragen?
Neem contact met ons op. U kunt ons bellen op het nummer 088-012 34 56. Wij zijn open van maandag tot en met vrijdag tussen 9.00 en 17.00 uur. Of kijk voor meer informatie op onze website. Wij helpen u graag.

Met vriendelijke groet,


A.B. van Dijk
Manager Heffen
De ambtenaar belast met de heffing`,
				"Belastingsamenwerking Zonnendaal", "Belastingsamenwerking Zonnendaal", "Belastingsamenwerking Zonnendaal", "Belastingsamenwerking Zonnendaal", "Belastingsamenwerking Zonnendaal"),
			Attachments: []attachment{
				{
					Label: fmt.Sprintf("WOZ-aanslag %d", bill.CreatedAt.Year()),
					URL:   fmt.Sprintf("%s/tax-bills/%s/download", config.Config.Self.Domain, bill.ID.String()),
				},
			},
		})
	})

	app.Get("/tax-bills/:id/download", func(c *fiber.Ctx) error {
		// Fetch the tax bill from the Belastingsamenwerking backend
		billID, err := uuid.Parse(c.Params("id"))
		if err != nil {
			return fmt.Errorf("uuid parse failed: %w", err)
		}

		// Note: We cannot use a byte stream to download the PDF directly into the ResponseWriter
		// Because this is not support by fiber
		file, err := os.CreateTemp("/tmp/", "temp-*.pdf")
		if err != nil {
			return nil
		}

		defer os.Remove(file.Name())

		if err := backend.Download(http.MethodGet, config.Config.Belastingsamenwerking.Domain, fmt.Sprintf("/tax-bills/%s/download", billID), map[string]string{"Fsc-Grant-Hash": config.Config.Belastingsamenwerking.FSCGrantHash}, file); err != nil {
			return fmt.Errorf("tax bill download request failed: %w", err)
		}

		return c.Download(file.Name(), fmt.Sprintf("aanslagbiljet-%s.pdf", time.Now().Format("2006-01-02T15-04-05")))
	})

	// Start server
	if err := app.Listen(config.Config.ListenAddress); err != nil { // Note: port should never be nil, since flag.Parse() is run
		log.Println("error starting server:", err)
	}
}
