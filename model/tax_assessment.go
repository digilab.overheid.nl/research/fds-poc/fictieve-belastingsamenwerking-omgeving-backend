package model

import (
	"time"

	"github.com/google/uuid"
)

type TaxAssessment struct {
	ID                uuid.UUID `json:"id"`
	TariffID          uuid.UUID `json:"tariffId"`
	WOZOrderID        uuid.UUID `json:"wozOrderId"`
	WOZObjectID       uuid.UUID `json:"wozObjectId"`
	OwnerType         string    `json:"ownerType"`
	OwnerID           uuid.UUID `json:"ownerId"`
	OwnerLabel        string    `json:"ownerLabel"`
	OwnerAddressID    uuid.UUID `json:"ownerAddressId"`
	OwnerAddressLine1 string    `json:"ownerAddressLine1"`
	OwnerAddressLine2 string    `json:"ownerAddressLine2"`
	WOZAddressID      uuid.UUID `json:"wozAddressId"`
	WOZAddressLine1   string    `json:"wozAddressLine1"`
	WOZAddressLine2   string    `json:"wozAddressLine2"`
	WOZAmount         int32     `json:"wozAmount"`
	RegisteredPeople  int32     `json:"registeredPeople"`
	CreatedAt         time.Time `json:"createdAt"`
}
