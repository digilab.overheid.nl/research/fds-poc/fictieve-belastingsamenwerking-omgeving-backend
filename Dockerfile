# Stage 1
FROM golang:1.21-alpine3.18 AS builder

# Copy the code into the container. Note: copy to a dir instead of `.`, since $GOPATH may not contain a go.mod file
WORKDIR /build
COPY . .

# Build the Go files
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o server .


# Stage 2
FROM alpine:3.18

# Add timezones
RUN apk add --no-cache tzdata

# Copy the binary from /build to the root folder of this container. Also copy the public dir that was built
COPY --from=builder /build/server /

# Run the binary when starting the container
ENTRYPOINT ["/server"]

EXPOSE 80
